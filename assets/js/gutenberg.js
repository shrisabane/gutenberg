let call = false;
let load_item = '';

;(function($){
$.fn.extend({
    donetyping: function(callback,timeout){
        timeout = timeout || 1e3; // 1 second default timeout
        var timeoutReference,
            doneTyping = function(el){
                if (!timeoutReference) return;
                timeoutReference = null;
                callback.call(el);
            };
        return this.each(function(i,el){
            var $el = $(el);
            // Chrome Fix (Use keyup over keypress to detect backspace)
            // thank you @palerdot
            $el.is(':input') && $el.on('keyup keypress paste',function(e){
                // This catches the backspace button in chrome, but also prevents
                // the event from triggering too preemptively. Without this line,
                // using tab/shift+tab will make the focused element fire the callback.
                if (e.type=='keyup' && e.keyCode!=8) return;
                
                // Check if timeout has been set. If it has, "reset" the clock and
                // start over again.
                if (timeoutReference) clearTimeout(timeoutReference);
                timeoutReference = setTimeout(function(){
                    // if we made it here, our timeout has elapsed. Fire the
                    // callback
                    doneTyping(el);
                }, timeout);
            }).on('blur',function(){
                // If we can, fire the event since we're leaving the field
                doneTyping(el);
            });
        });
    }
});
})(jQuery);

$(document).ready(function(){
    var URL = window.location.href;   
    if(method == 'book_list'){
        showLoader(); 
        let API = base_url + '?genre=' + genre;
        load_data(API);       
    }
});

$('.genrecard').click( function(){
    let text = $(this).children('.font-20').text();
    console.log(text);

    window.location.href = window.location.href + "/book_list?genre=" + text;

});

$(document).on("click", ".book_cover", function(){
    let url = $(this).attr('data-link');
    console.log(url);
    (url != '') ? window.open(url, "_blank") : alert('No viewable version available.');    
}); 

$('#search_box').donetyping(function(){
    let name = $("#search_box").val();
    if(name.length >= 4 && call == false){
        $("#books").html("");
        load_item = '';
        showLoader(); 
        let API = base_url + '?genre=' + genre + '&search=' + name;
        if(API != 'null' && API != null && API != undefined){ 
            call = true;  
            load_data(API);  
        }
    } else if(name.length == 0 && call == false){
        $("#books").html("");
        load_item = '';
        showLoader(); 
        let API = base_url + '?genre=' + genre + '&search=' + name;
        if(API != 'null' && API != null && API != undefined){ 
            call = true;  
            load_data(API);  
        }
    }
});

$(window).scroll(function() {
    let position = $(window).scrollTop() + $(window).height();
    let bottom = $("#books").height();    
    
    if(position > bottom && call == false){
        next_page_data();
    }  

});

function next_page_data(){
    let API = $('#next').val();  
    if(API != 'null' && API != null && API != undefined){ 
            call = true;  
            load_data(API);  
    }
}

function load_data(API){
    let dataString = '';
    $.ajax({
        type: "GET",
        url: API,
        data: dataString,
        dataType: "json",
        beforeSend: function() {
          showLoader();
        },
        complete: function() {
            hideLoader();
            call = false; 
            console.log(load_item);
            if(load_item < 13){
               next_page_data(); 
            }
        },
        success: function(result){
          result = JSON.parse(JSON.stringify(result));         
          console.log(result);
            if(result.count > 0){
                showLoader();
                let books = result.results;
                let next  = result.next;
                let previous  = result.previous; 

                $("#next").val(next);
                $("#previous").val(previous);

                $.each(books, function(key, value) {
                    let img = '';
                    let author = '';
                    let format = '';
                    let img_link = '';
                    let web_link = '';
                    if(books[key].authors != undefined){
                    author = (books[key].authors.name != '') ? books[key].authors.name : '';
                    } 

                    if(books[key].formats != undefined){                    
                        img = Object.values(books[key].formats);
                        //console.log(img);
                        $.each(img, function(k, v) {
                            if(is_image(String(v))){
                                img_link = v;
                            } else if(is_weblink(String(v))){                                
                                if(!web_link.includes(".htm")){
                                    web_link = v;
                                }
                            }
                        });
                    }
                    if(img_link != ''){
                        $("#books")
                            .append('<div class="col-lg-2 col-sm-2 col-4"><div class="book_cover" data-link="'+ web_link +'"><img class="Rectangle" src="' + img_link + '"> <div> <div class="font-12 cardbook">' + value.title.substring(0, 45) + '</div> <div class="font-12 grey">' + author + '</div></div></div></div>');
                        load_item++;
                    }
                });        
            } else {
                $("#books").append('<div> No Data Found </div>');
            }
        }
        
     });
}

function goBack() {
    window.history.back();
}

function showLoader() {
    $('#preloader').css('display', 'block');
    $('#status').css('display', 'block');
    $('html, body').css({'overflow':'hidden', 'position':'relative', 'min-height':'100%', 'top':'0' });
}

function hideLoader() {
    $('#preloader').css('display', 'none');
    $('#status').css('display', 'none');
    $('html, body').css({'overflow':'visible', 'display': 'block' });
}

function is_image(fileName) {
    var extension = fileName.substring(fileName.lastIndexOf('.') + 1); 
    switch(extension) {
        case 'jpg':
        case 'png':
        case 'gif':
            return true;
        break;                     
        case 'zip':
        case 'rar':
            return false;
        break;
        case 'pdf':
            return false;
        break;
        default:
            return false;
    }
}

function is_weblink(fileName) {
    if(fileName.includes(".htm") || fileName.includes(".pdf") || fileName.includes('.txt')){
        return true;
    } else {
        return false;
    }
}