<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Books extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method with & without filters on book data.
     *
     * @return Response
    */
	public function index_get()
	{
        $limit = 25; // data limit for fetching data from DB
		$page = (!empty($this->input->get('page'))) ? $this->input->get('page') : 0; // page number
		$offset = $page*$limit; // offset for fetching data from DB
		
		//$this->pre($_GET, 1);

		// count code block start here
		if(!empty($this->input->get())) {
			$this->db->select('count(distinct(b.gutenberg_id)) as count');
			$this->db->from('books_book b');
			$this->db->join('books_format bf', 'b.gutenberg_id = bf.book_id', 'LEFT');
			$this->db->join('books_book_authors bba', 'b.gutenberg_id = bba.book_id', 'LEFT');
			$this->db->join('books_author ba', 'bba.author_id = ba.id', 'LEFT');
			$this->db->join('books_book_subjects bbs', 'bbs.book_id = b.gutenberg_id', 'LEFT');
			$this->db->join('books_subject bs', 'bbs.subject_id = bs.id', 'LEFT');
			$this->db->join('books_book_bookshelves bbsh', 'bbsh.book_id = b.gutenberg_id', 'LEFT');
			$this->db->join('books_bookshelf bsh', 'bbsh.bookshelf_id = bsh.id', 'LEFT');
			$this->db->join('books_book_languages bbl', 'bbl.book_id = b.gutenberg_id', 'LEFT');
			$this->db->join('books_language bl', 'bl.id = bbl.language_id', 'LEFT');

			if(!empty($this->input->get('genre'))) {			
				$this->db->like('LOWER(bsh.name)', strtolower($this->input->get('genre')));
			}
			
			if(!empty($this->input->get('search'))){
				$search_words = $this->input->get('search');
				$this->db->like('title', $search_words);
				$this->db->or_like('ba.name', $search_words);
			}

			(!empty($this->input->get('topic'))) ? $this->db->or_like('bs.name', $this->input->get('topic')) : '';
			(!empty($this->input->get('topic'))) ? $this->db->or_like('bsh.name', $this->input->get('topic')) : '';
			
			(!empty($this->input->get('ids'))) ? $this->db->where_in('gutenberg_id', explode(',', $this->input->get('ids'))) : '';
			(!empty($this->input->get('languages'))) ? $this->db->where_in('code', explode(',', $this->input->get('languages'))) : '';
			(!empty($this->input->get('mime_type'))) ? $this->db->where_in('mime_type', explode(',', $this->input->get('mime_type'))) : '';

			$data = $this->db->get()->row_array();

		} else {
			$this->db->select('count(*) as count');
			$data = $this->db->get('books_book')->row_array();
        }
        
        // count code block ends here 
        
        // next & previous links code start here
		$total_rows = $data['count'];
		$total_pages = ceil($total_rows / $limit);
		$query =  $this->input->get();

		if($page <= 1 && $total_pages <= 1){
			$data['next'] = 'null';
			$data['previous'] = 'null';
		} else if($page <= 1 && $total_pages > 1){
			
			$query['page'] = $page+1;
			$query_result = http_build_query($query);
			$data['next'] = base_url().'?'.$query_result;
			$data['previous'] = 'null';			
		} else if($page > 1 && $page < $total_pages) {

			$query['page'] = $page-1;
			$query_result = http_build_query($query);
			$data['previous'] = base_url().'?'.$query_result;

			$query['page'] = $page+1;
			$query['page'] = ($query['page'] >= $total_pages) ? $total_pages : $query['page'];
			$query_result = http_build_query($query);
			$data['next'] = base_url().'?'.$query_result;	
		
		} else if($page > 1 && $page >= $total_pages) {
			$data['next'] = 'null';		

			$query['page'] = $page-1;
			$query_result = http_build_query($query);
			$data['previous'] = base_url().'?'.$query_result;
		} else {
			$data['next'] = 'null';
			$data['previous'] = 'null';
        }        
        // Next Previous links code ends here

		// result data code block start here
		$this->db->select('distinct(b.gutenberg_id) as id, b.title as title, GROUP_CONCAT(distinct(concat("name", "|",  ba.name, "~", "birth_year", "|", ba.birth_year, "~", "death_year", "|", ba.death_year)) SEPARATOR ",") as authors, GROUP_CONCAT(distinct(bs.name) SEPARATOR ",") as subjects, GROUP_CONCAT(distinct(bsh.name) SEPARATOR ",") as bookshelves, GROUP_CONCAT(distinct(bl.code) SEPARATOR ",") as languages, "false" AS copyright, b.media_type, GROUP_CONCAT(distinct(concat(bf.mime_type, "|", bf.url)) SEPARATOR "~") as formats, b.download_count');
		$this->db->from('books_book b');
		$this->db->join('books_format bf', 'b.gutenberg_id = bf.book_id', 'LEFT');
		$this->db->join('books_book_authors bba', 'b.gutenberg_id = bba.book_id', 'LEFT');
		$this->db->join('books_author ba', 'bba.author_id = ba.id', 'LEFT');
		$this->db->join('books_book_subjects bbs', 'bbs.book_id = b.gutenberg_id', 'LEFT');
		$this->db->join('books_subject bs', 'bbs.subject_id = bs.id', 'LEFT');
		$this->db->join('books_book_bookshelves bbsh', 'bbsh.book_id = b.gutenberg_id', 'LEFT');
		$this->db->join('books_bookshelf bsh', 'bbsh.bookshelf_id = bsh.id', 'LEFT');
		$this->db->join('books_book_languages bbl', 'bbl.book_id = b.gutenberg_id', 'LEFT');
		$this->db->join('books_language bl', 'bl.id = bbl.language_id', 'LEFT');

		if(!empty($this->input->get('genre'))) {			
			$this->db->like('LOWER(bsh.name)', strtolower($this->input->get('genre')));
		}
		
		if(!empty($this->input->get('search'))){
			$search_words = $this->input->get('search');
			$this->db->like('title', $search_words);
			$this->db->or_like('ba.name', $search_words);
		}

		(!empty($this->input->get('topic'))) ? $this->db->or_like('bs.name', $this->input->get('topic')) : '';
		(!empty($this->input->get('topic'))) ? $this->db->or_like('bsh.name', $this->input->get('topic')) : '';

		(!empty($this->input->get('ids'))) ? $this->db->where_in('gutenberg_id', explode(',', $this->input->get('ids'))) : '';
		(!empty($this->input->get('languages'))) ? $this->db->where_in('code', explode(',', $this->input->get('languages'))) : '';
		(!empty($this->input->get('mime_type'))) ? $this->db->where_in('mime_type', explode(',', $this->input->get('mime_type'))) : '';

		$this->db->order_by('download_count', 'DESC');
		$this->db->group_by('id'); 
		$this->db->limit($limit, $offset);
		$data['results'] = $this->db->get()->result_array();

		//$this->pre($data);
		// data manupulation from string to array block starts here			
		foreach($data['results'] as $key => $value){
			if(!empty($value['formats'])){
				$formats = explode('~', $value['formats']);
				foreach($formats as $Ckey => $Cval){
					$semi = explode('|', $Cval);
					$formats2[$key][$semi[0]] = $semi[1];
				}			
				$data['results'][$key]['formats'] = $formats2[$key];
			}
			if(!empty($value['authors'])){
				$authors = explode('~', $value['authors']);
				foreach($authors as $Ckey => $Cval){
					$semi = explode('|', $Cval);
					$authors2[$key][$semi[0]] = $semi[1];
				}			
                $data['results'][$key]['authors'] = $authors2[$key];
			}
		}
		// data manupulation from string to array block ends here	
		
		// result data code block ends here

        $this->response($data, REST_Controller::HTTP_OK);
    }
    
    /**
     * prints the array in readable format
     * @param array $data 
     * @param bool $die whether to stop execution of script after array print
     * @return type
     */
    function pre($data,$die = true)
    {
        echo '<pre>';print_r($data);echo '</pre>';
        if($die){ die(); }
    }
          	
}