<?php

class Home extends CI_Controller {

        public function __construct()
        {
                parent::__construct(); 
                // Your own constructor code
        }

        public function index(){
            $data['genre'] = array(
                                '0' => array('name'=> 'FICTION',    'img_link' => 'assets/img/Fiction.svg'   ), 
                                '1' => array('name'=> 'DRAMA',      'img_link' => 'assets/img/Drama.svg'     ), 
                                '2' => array('name'=> 'HUMOUR',     'img_link' => 'assets/img/Humour.svg'    ), 
                                '3' => array('name'=> 'POLITICS',   'img_link' => 'assets/img/Politics.svg'  ), 
                                '4' => array('name'=> 'PHILOSOPHY', 'img_link' => 'assets/img/Philosophy.svg'), 
                                '5' => array('name'=> 'HISTORY',    'img_link' => 'assets/img/History.svg'   ), 
                                '6' => array('name'=> 'ADVENTURE',  'img_link' => 'assets/img/Adventure.svg' )
                            );


            $this->load->view('home_view', $data);
        }

        public function book_list(){
            $data['genre'] = (!empty($this->input->get('genre'))) ? ($this->input->get('genre')) : 'Fiction';
            $this->load->view('list_view', $data);
        }

}