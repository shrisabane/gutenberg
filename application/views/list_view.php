<!DOCTYPE html>
<html lang="en">
<head> 
  <base href="<?= base_url(); ?>">
  <title>Gutenberg Project</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<div class="container-fluid bg-white">
<div class="container pd-head bg-white">
  <h1 class="subheading"><img src="assets/img/Back.svg" class="back-button img-fluid" onclick="goBack()" > <?= $genre ?> </h1>
    <div class="form-group has-search">
        <span class="fa fa-search form-control-feedback"></span>
        <input type="search" class="form-control" id="search_box" placeholder="Search author names and book titles">
    </div>   
</div>
</div>
<div class="container genrecardpd">
  <div class="row" id="books"></div>
  <input type="hidden" id="next" value="" />
  <input type="hidden" id="previous" value="" /> 
</div>

<script type="text/javascript"> 
    var genre = '<?= $genre ?>';
    var base_url = '<?= base_url() ?>';
    var method = '<?= $this->uri->segment(2); ?>';
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="assets/js/gutenberg.js?v=<?php echo strtotime('now'); ?>"></script>

</body>
</html>
