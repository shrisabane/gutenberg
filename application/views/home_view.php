<!DOCTYPE html>
<html lang="en">
<head>
  <base href="<?= base_url(); ?>">
  <title>Gutenberg Project</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<div class="container-fluid bg-head">
<div class="container pd-head ">
  <h1 class="heading">Gutenberg Project</h1>
  <p class="font-regular font-16">A social cataloging website that allows you to freely search its database of books, annotations,
    and reviews.</p> 
</div>
</div>
<div class="container genrecardpd">
  <div class="row">
      <?php foreach($genre as $key => $value){ ?>
      <div class="col-md-6">          
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">          
            <ul class="list-inline genrecard">
                <li class="list-inline-item"><img src="<?= $value['img_link'] ?>" class="imgicon img-fluid"></li>
                <li class="list-inline-item font-20"><?= $value['name'] ?></li>
                <li class="list-inline-item float-right"><img src="assets/img/Next.svg" class="img-next img-fluid"></li>
            </ul>
        </div>
      </div> 
      <?php } ?>
  </div>
</div>
<script type="text/javascript"> 
    var base_url = '<?= base_url() ?>';
    var method = '<?= $this->uri->segment(2); ?>';
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="assets/js/gutenberg.js?v=<?php echo strtotime('now'); ?>"></script>

</body>
</html>
